import 'dart:async';

import 'package:biomoneda_app/feature/login/screen/bloc/login_bloc.dart';
import 'package:get_it/get_it.dart';

final sl = GetIt.instance;
Future<void> init() async {
  sl..registerFactory<LoginBloc>(() => LoginBloc());
}
