import 'package:biomoneda_app/core/widgets/login/register_form.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Registrate')
      ),
      body: SingleChildScrollView(
        child: RegisterForm(),
      ),
    );
  }
}
