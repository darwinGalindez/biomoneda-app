import 'package:biomoneda_app/core/base/base_page.dart';
import 'package:biomoneda_app/core/widgets/login/login_form.dart';
import 'package:biomoneda_app/feature/login/screen/bloc/login_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:biomoneda_app/injection_container.dart';

class LoginPage extends BasePage<LoginState, LoginBloc> {

  const LoginPage({
    super.key
  });

  @override
  Widget buildPage(BuildContext context, LoginState state, LoginBloc bloc) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width, // Ajusta el ancho de la imagen al ancho de la pantalla
              height: MediaQuery.of(context).size.height * 0.5, // Ajusta la altura de la imagen a la mitad de la pantalla
              child: Image.asset(
                'assets/images/login.jpg', // Ruta de la imagen
                fit: BoxFit.cover, // Ajusta la imagen para cubrir el espacio proporcionado
              ),
            ),
            // LoginForm(),
          ],
        ),
      ),
    );
  }

  @override
  LoginBloc createBloc(BuildContext context) => sl<LoginBloc>()..init();
}