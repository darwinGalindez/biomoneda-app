part of 'login_bloc.dart';

class LoginState {

  LoginState({
    required this.email,
    required this.password
  });

  final String email;
  final String password;

  factory LoginState.init() => LoginState(email: '', password: '');
}