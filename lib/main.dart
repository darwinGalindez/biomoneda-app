import 'package:biomoneda_app/core/navigator/app_navigator.dart';
import 'package:flutter/material.dart';
import 'package:biomoneda_app/injection_container.dart' as getIt;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    getIt.init();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorKey: AppNavigator.navigatorKey,
      onGenerateRoute: AppNavigator.generateRoute
    );
  }
}
