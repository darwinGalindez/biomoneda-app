import 'package:flutter/material.dart';

class RegisterForm extends StatefulWidget {
  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _formKey = GlobalKey<FormState>();
  String _name = '';
  String _lastName = '';
  String _email = '';
  String _bithDate = '';
  String _documentType = '';
  String _documentNumber = '';
  String _password = '';
  String _passwordConfirm = '';

  void _submitForm() {
    if (_formKey.currentState!.validate()) {
      // Aquí puedes implementar la lógica de autenticación
      // Por ahora, simplemente imprimimos los valores ingresados
      print('Nombres: $_name');
      print('Apellidos: $_lastName');
      print('Email: $_email');
      print('Fecha de nacimiento: $_bithDate');
      print('Tipo de documento: $_documentType');
      print('Número de documento: $_documentNumber');
      print('Contraseña: $_password');
      print('Confirmar contraseña: $_passwordConfirm');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: EdgeInsets.all(35.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
              decoration: InputDecoration(labelText: 'Nombres'),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Por favor, ingresa tu nombre';
                }
                return null;
              },
              onChanged: (value) {
                setState(() {
                  _name = value;
                });
              },
            ),
            SizedBox(height: 10.0),
            TextFormField(
              decoration: InputDecoration(labelText: 'Apellidos'),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Por favor, ingresa tus apellidos';
                }
                return null;
              },
              onChanged: (value) {
                setState(() {
                  _lastName = value;
                });
              },
            ),
            SizedBox(height: 10.0),
            TextFormField(
              decoration: InputDecoration(labelText: 'Correo eletrónico'),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Por favor, ingresa tu correo electrónico';
                }
                return null;
              },
              onChanged: (value) {
                setState(() {
                  _email = value;
                });
              },
            ),
            SizedBox(height: 10.0),
            TextFormField(
              decoration: InputDecoration(labelText: 'Fecha de nacimiento'),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Por favor, ingresa tu fecha de nacimiento';
                }
                return null;
              },
              onChanged: (value) {
                setState(() {
                  _bithDate = value;
                });
              },
            ),
            SizedBox(height: 10.0),
            TextFormField(
              decoration: InputDecoration(labelText: 'Tipo de documento'),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Por favor, seleccione su tipo de documento';
                }
                return null;
              },
              onChanged: (value) {
                setState(() {
                  _documentType = value;
                });
              },
            ),
            SizedBox(height: 10.0),
            TextFormField(
              decoration: InputDecoration(labelText: 'Número de documento'),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Por favor, ingresa tu número de documento';
                }
                return null;
              },
              onChanged: (value) {
                setState(() {
                  _documentNumber = value;
                });
              },
            ),
            SizedBox(height: 10.0),
            TextFormField(
              decoration: InputDecoration(labelText: 'Contraseña'),
              obscureText: true,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Por favor, ingresa tu contraseña';
                }
                return null;
              },
              onChanged: (value) {
                setState(() {
                  _password = value;
                });
              },
            ),
            SizedBox(height: 10.0),
            TextFormField(
              decoration: InputDecoration(labelText: 'Confirmar contraseña'),
              obscureText: true,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Por favor, confirma tu contraseña';
                }
                return null;
              },
              onChanged: (value) {
                setState(() {
                  _passwordConfirm = value;
                });
              },
            ),
            SizedBox(height: 40.0),
            SizedBox(
                width: double.infinity,
                height: 45.0,
                child: ElevatedButton(
                  onPressed: _submitForm,
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.green
                  ),
                  child: Text(
                    'Registrar',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0
                    ),
                  ),
                )
            )
          ],
        ),
      ),
    );
  }
}
